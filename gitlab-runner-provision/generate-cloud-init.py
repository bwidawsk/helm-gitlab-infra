#!/usr/bin/env python3
#
# Copyright © 2020 Daniel Stone
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
# DEALINGS IN THE SOFTWARE.
#
# Authors: Daniel Stone <daniel@fooishbar.org>

import click
import os
import os.path
import requests
import yaml

# use literal rather than quoted encoding for block strings
# https://stackoverflow.com/a/33300001
def str_presenter(dumper, data):
    if len(data.splitlines()) > 1:  # check for multiline string
        return dumper.represent_scalar("tag:yaml.org,2002:str", data, style="|")
    return dumper.represent_scalar("tag:yaml.org,2002:str", data)


yaml.add_representer(str, str_presenter)


class MyDumper(yaml.Dumper):
    def increase_indent(self, flow=False, indentless=False):
        return super(MyDumper, self).increase_indent(flow, False)


def get_local_file(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


def get_remote_uri(uri):
    get = requests.get(uri)
    if not get.ok:
        get.raise_for_status()
    return get.text


cloud_init = {
    "package-update": True,
    "package-upgrade": True,
    "apt": {"sources": {}},
    "packages": [
        # HTTPS archive verification
        "debian-archive-keyring",
        "apt-transport-https",
        "gnupg",
        # sensible updates
        "needrestart",
        # ??
        "patch",
        # network
        "wireguard",
        "ufw",
        # daniels
        "zsh",
        "vim",
        # bentiss
        "neovim",
        # misc
        "jq",
        "mdadm",
        "parted",
    ],
    "runcmd": [
        # the upgrade by cloud-init doesn't upgrade the kernel
        "env DEBIAN_FRONTEND=noninteractive apt-get update",
        "env DEBIAN_FRONTEND=noninteractive apt-get -y dist-upgrade",
        "env DEBIAN_FRONTEND=noninteractive apt-get install -y gnupg",
        # kernel-img.conf creates /boot/initrd.img when our grub expects /boot/initrd
        "if [ ! -f /boot/initrd.img ] ; then mv /boot/initrd /boot/initrd.img ; fi",
        "ln -sf initrd.img /boot/initrd",
        # force reboot to the new kernel:
        # the latest debian images on equinix do not include gnupg, meaning that
        # apt can not add the keys of the repos in cloud_init['apt']['sources'].
        # a reboot and restart of cloud-init solves that
        # we clean cloud-init data so that it re-runs next time
        "if [ ! -f /reboot-done ] ; then cloud-init clean ; rm -f /etc/apt/sources.list.d/*.list ; touch /reboot-done; reboot ; sleep 60 ; else rm /reboot-done ; fi",
    ],
    "write_files": [
        {
            "path": "/etc/kernel-img.conf",
            "owner": "root:root",
            "content": "do_symlinks=Yes\nimage_dest=/boot",
        },
        {
            "path": "/etc/podman-gc-exclude",
            "owner": "root:root",
            "content": get_local_file("podman-gc-exclude"),
        },
    ],
}


def add_dedicated_storage(gitlab_runner_arch):
    if gitlab_runner_arch == "x86-64":
        cloud_init["runcmd"].extend(
            [
                # configure RAID for Docker storage
                "mkdir -p /run/cloudinit",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | map_values("/dev/" + .name) | join(" ")' > /run/cloudinit/FREE_HDD""",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | map_values("/dev/" + .name + "p1") | join(" ")' >> /run/cloudinit/FREE_HDD_PART""",
                """lsblk --json -b | jq -r '.blockdevices | map(select(.children == null and (.name | startswith("nvme")))) | length' > /run/cloudinit/COUNT_HDD""",
                "echo FREE_HDD $(cat /run/cloudinit/FREE_HDD)",
                "echo FREE_HDD_PART $(cat /run/cloudinit/FREE_HDD_PART)",
                "echo COUNT_HDD $(cat /run/cloudinit/COUNT_HDD)",
                "for DEV in $(cat /run/cloudinit/FREE_HDD) ; do echo create partition on $DEV; echo label: gpt | sfdisk $DEV; echo type=R | sfdisk $DEV; done",
                "mdadm --create --verbose /dev/md0 --level=0 --raid-devices=$(cat /run/cloudinit/COUNT_HDD) $(cat /run/cloudinit/FREE_HDD_PART)",
                "mkfs.ext4 -F /dev/md0",
                "echo 'UUID=\"'$(blkid -s UUID -o value /dev/md0)'\" /var/lib/containers ext4 defaults 0 0' | tee -a /etc/fstab",
                "mkdir -p /var/lib/containers",
                "mount -a",
            ]
        )


def add_gitlab_runner(
    instance_name,
    registration_token,
    concurrent,
    gitlab_tags,
    maybe_untagged,
    gitlab_runner_arch,
):
    cloud_init["write_files"].append(
        {
            "path": "/etc/containers/registries.conf",
            "owner": "root:root",
            "permissions": "0644",
            "content": get_local_file("registries.conf"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/containers/containers.conf",
            "owner": "root:root",
            "permissions": "0644",
            "content": get_local_file("containers.conf"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/systemd/system/gitlab-runner.service.d/kill.conf",
            "owner": "root:root",
            "content": get_local_file("gitlab-runner-systemd-kill.conf"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/modprobe.d/vsock.conf",
            "owner": "root:root",
            "content": """
        blacklist vmw_vmci
        blacklist vmw_vsock_vmci_transport
        blacklist vmw_vsock_virtio_transport_common
        """,
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/modules-load.d/vsock.conf",
            "owner": "root:root",
            "content": "vhost_vsock",
        }
    )
    cloud_init["apt"]["sources"]["gitlab-runner.list"] = {
        "source": "deb https://packages.gitlab.com/runner/gitlab-runner/debian bullseye main",
        "key": get_remote_uri(
            "https://packages.gitlab.com/runner/gitlab-runner/gpgkey"
        ),
    }
    # for podman 4.3+
    if gitlab_runner_arch == "x86-64":
        cloud_init["apt"]["sources"]["podman-alvistack.list"] = {
            "source": "deb http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_11/ /",
            "key": get_remote_uri(
                "http://downloadcontent.opensuse.org/repositories/home:/alvistack/Debian_11/Release.key"
            ),
        }
    else:
        cloud_init["apt"]["sources"]["kubic_libcontainers_stable.list"] = {
            "source": "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/ /",
            "key": get_remote_uri(
                "https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/Debian_11/Release.key"
            ),
        }
    cloud_init["packages"].extend(["gitlab-runner"])
    # container runtime
    cloud_init["packages"].extend(
        ["podman", "slirp4netns", "fuse-overlayfs", "uidmap", "tini", "cgroupfs-mount"]
    )
    # needed for helper-image bootstrapping
    cloud_init["packages"].extend(
        ["binutils", "binutils-common", "cdebootstrap", "gettext-base"]
    )
    if "aarch64" in gitlab_tags:
        cloud_init["packages"].extend(["binutils-aarch64-linux-gnu"])
    else:
        cloud_init["packages"].extend(["binutils-x86-64-linux-gnu"])
    # support qemu for cross-arch building
    cloud_init["packages"].extend(["binfmt-support", "qemu-user-static"])

    if maybe_untagged:
        gitlab_untagged = "--run-untagged"
    else:
        gitlab_untagged = ""

    # pull static curl from https://github.com/moparisthebest/static-curl
    cloud_init["runcmd"].append("mkdir -p /var/host/bin")
    curl_url = f"https://github.com/moparisthebest/static-curl/releases/latest/download/curl-"
    if gitlab_runner_arch == "x86-64":
        curl_url += "amd64"
    else:
        curl_url += "aarch64"

    register_command = f"--name {instance_name}"
    register_command += f" --non-interactive"
    register_command += f" --limit {concurrent}"
    register_command += f" --request-concurrency 2"
    register_command += f" --executor docker"
    register_command += f" --docker-host unix:///run/podman/podman.sock"
    register_command += f" --docker-pull-policy if-not-present"
    register_command += f" --docker-image alpine:latest"
    register_command += f" --docker-privileged"
    register_command += f" --docker-devices /dev/kvm"
    register_command += f' --docker-volumes "/var/cache/gitlab-runner/cache:/cache"'
    register_command += f' --docker-volumes "/var/host:/host:ro"'
    register_command += f" --custom_build_dir-enabled=true"
    register_command += f" --registration-token {registration_token}"
    register_command += f' --env "DOCKER_TLS_CERTDIR="'
    register_command += f' --env "FDO_CI_CONCURRENT={concurrent}"'
    register_command += f" --tag-list {gitlab_tags} {gitlab_untagged}"
    register_command += f" --docker-tmpfs /tmp:rw,nosuid,nodev,exec,mode=1777"
    register_command += f' --docker-extra-hosts "ssh.tmate.io:192.0.2.1"'
    register_command += f" --url https://gitlab.freedesktop.org"
    register_command += f' --pre-get-sources-script "eval \\"$CI_PRE_CLONE_SCRIPT\\""'
    register_command += f' --pre-build-script "/host/bin/curl -s -L --cacert /host/ca-certificates.crt --retry 4 -f --retry-delay 60 https://gitlab.freedesktop.org/freedesktop/helm-gitlab-infra/-/raw/main/runner-gating/runner-gating.sh | sh"'

    cloud_init["runcmd"].extend(
        [
            "mkdir -p /var/host/bin",
            f"curl -L -o /var/host/bin/curl {curl_url}",
            "chmod +x /var/host/bin/curl",
            "cp /etc/ssl/certs/ca-certificates.crt /var/host/",
            "systemctl enable --now podman.socket",
            "systemctl enable --now gitlab-runner.service",
            f"mkdir -p /var/cache/gitlab-runner/cache",
            f"mkdir /etc/gitlab-runner",
            f'echo "concurrent = {concurrent}" > /etc/gitlab-runner/config.toml',
            """echo "listen_address = "\\"$(ip -j -4 addr show | jq -r '.[] | (.addr_info[] | .local) | select(startswith("10."))')":3807\\"" >> /etc/gitlab-runner/config.toml""",
            f"gitlab-runner register {register_command}",
        ]
    )


def add_podman_dfs(gitlab_runner_arch):
    cloud_init["write_files"].append(
        {
            "path": "/usr/local/sbin/podman-free-space",
            "owner": "root:root",
            "permissions": "0755",
            "content": get_local_file("podman-free-space.py"),
        }
    )
    cloud_init["write_files"].append(
        {
            "path": "/etc/systemd/system/podman-free-space.service",
            "owner": "root:root",
            "content": get_local_file("podman-free-space.service"),
        }
    )
    if gitlab_runner_arch == "x86-64":
        cloud_init["packages"].extend(
            [
                "python3-podman",
            ]
        )
    else:
        cloud_init["packages"].extend(
            [
                "python3-pip",
                "python3-pkg-resources=52.0.0-4",  # conflict with the podman repo
            ]
        )
        cloud_init["runcmd"].append("pip3 install podman")
    cloud_init["runcmd"].append("systemctl daemon-reload")
    cloud_init["runcmd"].append("systemctl enable --now podman-free-space.service")
    cloud_init["packages"].extend(
        [
            "python3-click",
            "python3-parse",
            "python3-git",
            "python3-yaml",
        ]
    )


@click.command()
@click.option(
    "--instance-name", type=click.STRING, required=True, help="Name for this instance"
)
@click.option(
    "--gitlab-runner-registration-token",
    type=click.STRING,
    required=True,
    help="gitlab-runner registration token",
)
@click.option(
    "--gitlab-runner-concurrent",
    type=click.INT,
    required=True,
    help="Number of gitlab-runner jobs to run concurrently",
)
@click.option(
    "--gitlab-runner-tags",
    type=click.STRING,
    help="gitlab-runner tag list (comma-separated)",
)
@click.option(
    "--gitlab-runner-untagged/--gitlab-runner-only-tagged",
    default=False,
    help="Whether to accept GitLab CI jobs with no specified tags",
)
@click.option(
    "--gitlab-runner-arch",
    type=click.STRING,
    help="gitlab-runner arch (x86-64 or aarch64)",
)
def main(
    instance_name,
    gitlab_runner_registration_token,
    gitlab_runner_concurrent,
    gitlab_runner_tags,
    gitlab_runner_untagged,
    gitlab_runner_arch,
):
    add_dedicated_storage(gitlab_runner_arch)
    add_gitlab_runner(
        instance_name,
        gitlab_runner_registration_token,
        gitlab_runner_concurrent,
        gitlab_runner_tags,
        gitlab_runner_untagged,
        gitlab_runner_arch,
    )
    add_podman_dfs(gitlab_runner_arch)

    print("#cloud-config")
    # these options make the result more human readable
    print(
        yaml.dump(
            cloud_init, Dumper=MyDumper, indent=2, width=9999, default_flow_style=False
        )
    )
    print("")


if __name__ == "__main__":
    main()
