#!/bin/sh

set -e
set -u
set -o pipefail

# you must set:
#  - $PACKET_PROJECT_ID to the fd.o CI Packet project
#  - $RUNNER_ARCH to x86-64 or aarch64
#  - $PACKET_DEVICE_NAME to the desired device name (e.g. fdo-packet-5)
#  - $GITLAB_RUNNER_REG_TOKEN to the shared-runner registration token from https://gitlab.freedesktop.org/admin/runners

case "$RUNNER_ARCH" in
  x86-64)
    PACKET_PLAN="m3.large.x86"
    PACKET_METRO="dc"
    PACKET_TAGS="ci,ci-x86"
    GITLAB_RUNNER_CONCURRENT=8
    GITLAB_RUNNER_TAGS="kvm,packet.net"
    GITLAB_RUNNER_UNTAGGED="--gitlab-runner-untagged"
    ;;
  aarch64)
    PACKET_PLAN="c3.large.arm64"
    PACKET_METRO="dc"
    PACKET_TAGS="ci,ci-aarch64"
    GITLAB_RUNNER_CONCURRENT=16
    GITLAB_RUNNER_TAGS="aarch64,kvm-aarch64"
    GITLAB_RUNNER_UNTAGGED="--gitlab-runner-only-tagged"
    ;;
  *)
    echo "unknown arch $RUNNER_ARCH"
    exit 1
    ;;
esac

if [ "$(metal device get --project-id $PACKET_PROJECT_ID -o json | jq -r '.[] | select(.hostname == "'$PACKET_DEVICE_NAME'") | .hostname')" == $PACKET_DEVICE_NAME ]; then
	echo "Device name $PACKET_DEVICE_NAME already taken"
	exit 1
fi

CLOUD_INIT="$(./generate-cloud-init.py --instance-name $PACKET_DEVICE_NAME --gitlab-runner-registration-token $GITLAB_RUNNER_REG_TOKEN --gitlab-runner-concurrent $GITLAB_RUNNER_CONCURRENT --gitlab-runner-tags $GITLAB_RUNNER_TAGS $GITLAB_RUNNER_UNTAGGED --gitlab-runner-arch $RUNNER_ARCH)"

DEVICE_ID="$(metal device create --project-id $PACKET_PROJECT_ID --hostname $PACKET_DEVICE_NAME --plan $PACKET_PLAN --metro $PACKET_METRO --tags $PACKET_TAGS --operating-system debian_11 --userdata "${CLOUD_INIT}" --output json | jq -r .id)"

echo "Device $DEVICE_ID successfully created"

DEVICE_STATE=""

while ! [ "$DEVICE_STATE" = "active" ]; do
	sleep 10
	DEVICE_STATE="$(metal device get --id $DEVICE_ID -o json | jq -r .state)"
done

echo "Device $DEVICE_ID became active"


DEVICE_IP="$(metal device get --id $DEVICE_ID -o json | jq -r '.ip_addresses[] | select(.address_family == 4 and (.address | startswith("10.") | not)) | .address')"
echo "IP: $DEVICE_IP"

SOS_ADDR="${DEVICE_ID}@sos.$(metal device get --id $DEVICE_ID -o json | jq -r .facility.code).platformequinix.com"
echo "Remote console: $SOS_ADDR"
