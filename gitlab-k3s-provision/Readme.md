# Description

This will create a k3s cluster on packet (equinix metal) with the following:
- use of `c2.medium.x86` packet devices
- HA control plane
- multiple agents
- inter-agent communication through `wireguard` (flannel-wireguard extension)
- `kilo` as a VPN server to add peers to configure the cluster
- `equinix-metal-ccm` + `kube-vip` to have a LoadBalancer and automatic Elastic IPs
- `rook+ceph` to provide a CSI with the local disks on the server/agents

# Prerequesites

## Connect the local machine to the cluster:

```bash
./fdo-infra add-peer $USER-$(hostname) > fdo-k3s.conf

chmod 600 fdo-k3s.conf
wg-quick up ./fdo-k3s.conf

# fetch a k3s config file
mkdir -p ~/.kube/
./scripts/mk_kubeconfig.sh $USER-$(hostname) > ~/.kube/k3s.yaml

KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods
```

# Access the rook+ceph dashboard

The address of the dashboard can be retrieve with the following:

```bash
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get svc rook-ceph-mgr-dashboard

# get the password
KUBECONFIG=~/.kube/k3s.yaml kubectl -n rook-ceph get secrets \
    rook-ceph-dashboard-password -o jsonpath='{.data.password}' \
    | base64 -d
```

Then open a browser to the `CLUSTER-IP` shown above.

# Add an agent to the cluster
```bash
export PACKET_PROJECT_ID=@REPLACE_ME@
export PACKET_PROJECT_TOKEN=@REPLACE_ME@

./gitlab-k3s-packet.sh

# wait for the host to be ready

# attach the agent to the cluster
./scripts/register_agent.sh $PACKET_DEVICE_NAME

# watch the pods getting ready
KUBECONFIG=~/.kube/k3s.yaml kubectl get -A pods -o wide -w
```

# Re-deploy the k3s server
Note this will create an entire new cluster

```bash
export PACKET_PROJECT_ID=@REPLACE_ME@
export PACKET_PROJECT_TOKEN=@REPLACE_ME@

PACKET_DEVICE_NAME=fdo-k3s-server-1 ./gitlab-k3s-packet.sh --server
PACKET_DEVICE_NAME=fdo-k3s-server-2 ./gitlab-k3s-packet.sh --server
PACKET_DEVICE_NAME=fdo-k3s-server-3 ./gitlab-k3s-packet.sh --server

# wait for the hosts to be ready
./fdo-infra.py cluster-init fdo-k3s-server-1

# wait a few seconds for kilo to be applied
./fdo-infra.py add-peer --server fdo-k3s-server-1 $USER-$(hostname) > fdo-k3s.conf

chmod 600 fdo-k3s.conf
wg-quick up ./fdo-k3s.conf

# wait for the server to expose the control plane virtual IP
ping $(grep control_plane_eip config.yaml | sed 's/.*: //')
./fdo-infra.py server-join fdo-k3s-server-2
./fdo-infra.py server-join fdo-k3s-server-3
```
