apiVersion: batch/v1
kind: Job
metadata:
  labels:
    app.kubernetes.io/name: influxdb2-create-buckets
  name: influxdb2-create-buckets
spec:
  template:
    metadata:
      labels:
        app.kubernetes.io/name: influxdb2-create-buckets
    spec:
      serviceAccountName: job-influxdb2-init
      initContainers:
      - name: create-admin-credentials
        image: "bitnami/kubectl:{{ .Values.kubectlVersion }}"
        volumeMounts:
        - name: secrets
          mountPath: /secrets
          readOnly: false
        command:
        - /bin/sh
        args:
        - -c
        - |
          set -x;
          kubectl -n {{ .Values.namespace }} get secret influxdb2-auth-overwrite
          if [ "$?" -ne "0" ]
          then
            # secret doesn't exist, generate a new one
            openssl rand -base64 32 > /secrets/admin-password
            openssl rand -base64 32 > /secrets/admin-token
            kubectl -n {{ .Values.namespace }} create secret generic influxdb2-auth-overwrite \
              --from-file=admin-password=/secrets/admin-password \
              --from-file=admin-token=/secrets/admin-token
          fi
      - name: wait-for-influxdb
        image: "bitnami/kubectl:{{ .Values.kubectlVersion }}"
        command:
        - /bin/sh
        args:
        - -c
        - |
          set -x;
          while [ $(curl -sw '%{http_code}' "http://influxdb2.{{ .Release.Namespace }}.svc" -o /dev/null) -ne 200 ]; do
            sleep 15;
          done
      - name: influxdb2-create-auth
        image: "quay.io/influxdb/influxdb:{{ .Values.influxdb2Version }}"
        env:
          - name: INFLUXDB_PASSWORD
            valueFrom:
              secretKeyRef:
                name: influxdb2-auth-overwrite
                key: admin-password
          - name: INFLUXDB_TOKEN
            valueFrom:
              secretKeyRef:
                name: influxdb2-auth-overwrite
                key: admin-token
        volumeMounts:
          - name: secrets
            mountPath: /secrets
            readOnly: false
        command:
          - bash
        args:
          - -c
          - |
            ORG={{ .Values.organization }}

            if [ -e /secrets/admin-password ]
            then
                influx setup -f \
                  --host http://influxdb2:80 \
                  -o $ORG \
                  -b default \
                  -u admin \
                  -p ${INFLUXDB_PASSWORD} \
                  -t ${INFLUXDB_TOKEN}
            fi

            ORGID=$(influx \
                    --host http://influxdb2:80 \
                    -t ${INFLUXDB_TOKEN} \
                    org list \
                    -n $ORG \
                    --hide-headers | \
                    awk '{ print $1; }')

            {{- range .Values.buckets }}

            ##
            # processing {{ .name }}
            ##

            # create the bucket if it doesn't exist
            echo create bucket for {{.name}}
            influx \
              --host http://influxdb2:80 \
              -o $ORG \
              -t ${INFLUXDB_TOKEN} \
              bucket create \
              --hide-headers \
              -n {{ .name }}

            # retrieve the bucket ID
            BUCKETID=$(influx \
                       --host http://influxdb2:80 \
                       -o $ORG \
                       -t ${INFLUXDB_TOKEN} \
                       bucket list \
                       --hide-headers | \
                       grep {{ .name }} | \
                       awk '{ print $1; }')

            echo {{ .name }} $BUCKETID

            # check if there is a token attached to it
            TOKEN=$(influx \
                    --host http://influxdb2:80 \
                    -t ${INFLUXDB_TOKEN} \
                    auth list | \
                    grep {{ .name }} | \
                    grep -v write:users)  # this last grep helps excluding the admin's token
                                          # in case `.name` is in [telegraf, dahsboard, etc...]

            if [ -z "$TOKEN" ]
            then
              echo create token for {{ .name }}
              TOKEN=$(influx \
                      --host http://influxdb2:80 \
                      -t ${INFLUXDB_TOKEN} \
                      auth create \
                        -d {{ .name }} \
                        -o $ORG \
                        --read-bucket $BUCKETID \
                        --write-bucket $BUCKETID | \
                     grep {{ .name }})
            fi

            echo $TOKEN
            echo $TOKEN | grep {{ .name }} | awk '{ printf $3; }' > /secrets/{{ .name }}

            {{- end }}

            ##
            # grafana token can read everything in $ORG
            ##
            TOKEN=$(influx \
                    --host http://influxdb2:80 \
                    -o $ORG \
                    -t ${INFLUXDB_TOKEN} \
                    auth list | grep grafana)
            if [ -z "$TOKEN" ]
            then
              echo create token for grafana
              TOKEN=$(influx \
                      --host http://influxdb2:80 \
                      -o $ORG \
                      -t ${INFLUXDB_TOKEN} \
                      auth create \
                        --read-buckets \
                        -d grafana | \
                      grep grafana)
            fi

            echo $TOKEN
            echo $TOKEN | grep grafana | awk '{ printf $3; }' > /secrets/grafana
      containers:
      - name: create-secrets
        image: "bitnami/kubectl:{{ .Values.kubectlVersion }}"
        volumeMounts:
        - name: secrets
          mountPath: /secrets
          readOnly: true
        command:
        - /bin/sh
        args:
        - -c
        - |
          set -x;
          kubectl -n {{ .Values.namespace }} create secret generic influxdb2-tokens \
            --save-config --dry-run=client \
            --from-file=/secrets/grafana \
            {{- range .Values.buckets }}
            --from-file=/secrets/{{ .name }} \
            {{- end }}
            -o yaml | kubectl apply -f -
      volumes:
      - name: secrets
        emptyDir:
          medium: "Memory"
      restartPolicy: OnFailure
